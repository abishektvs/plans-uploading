import requests
import json


def update_status(
    web_hook_info,
    current_s3_key,
    status=None,
    usable_file=False,
    plf_sheet_data=None,
    plf_sheet_number=None,
):
    token = web_hook_info["user_token"]
    host = web_hook_info["file_uploaded_host"]

    url = f"https://{host}/api/v1/plan/file/detail/"
    payload = {"plf_s3_key": current_s3_key, "plf_usable_file": usable_file}

    if status is not None:
        payload["plf_status"] = status

    if plf_sheet_data is not None:
        payload["plf_sheet_data"] = plf_sheet_data

    if plf_sheet_number is not None:
        payload["plf_sheet_number"] = plf_sheet_number

    headers = {
        "Authorization": f"Bearer {token}",
        "Content-Type": "application/json",
    }

    response = requests.request("PATCH", url, headers=headers, data=json.dumps(payload))

    print("update_status_code", response.status_code)
    # print(response.text)


def post_trigger_generated_file(web_hook_info, batch_id, payload):
    token = web_hook_info["user_token"]
    host = web_hook_info["file_uploaded_host"]

    url = f"https://{host}/api/v1/plan/batch/{batch_id}/files/lambda-upload/"

    payload = payload
    headers = {
        "Authorization": f"Bearer {token}",
        "Content-Type": "application/json",
    }

    response = requests.request("POST", url, headers=headers, data=json.dumps(payload))

    print("post_genfilestatus_code", response.status_code)
    if response.status_code not in [200, 201]:
        # print(response.text)
        pass
    print("update_status_code", response.status_code)


def plan_file_detail(web_hook_info, file_s3_key):
    token = web_hook_info["user_token"]
    host = web_hook_info["file_uploaded_host"]

    url = f"https://{host}/api/v1/plan/file/detail/?file_s3_key={file_s3_key}"

    payload = {}
    headers = {
        "Authorization": f"Bearer {token}",
        "Content-Type": "application/json",
    }

    response = requests.request("GET", url, headers=headers, data=payload)

    # print(response.text)
    print("update_status_code", response.status_code)
    return response.json()


def update_set_index_data(web_hook_info, psm_id, psm_plans_index_sheet_data):
    token = web_hook_info["user_token"]
    host = web_hook_info["file_uploaded_host"]

    url = f"https://{host}/api/v1/plan/set/{psm_id}/"

    payload = {"psm_plans_index_sheet_data": psm_plans_index_sheet_data}
    headers = {
        "Authorization": f"Bearer {token}",
        "Content-Type": "application/json",
    }
    response = requests.request("PATCH", url, headers=headers, data=json.dumps(payload))

    print("update_set_status_code", response.text)
    return response.json()


def get_user_token(file_uploaded_host):
    host = file_uploaded_host
    url = f"https://{host}/api-token-auth/"

    if host == "dev-api.linarc.io":
        payload = {"username": "lordv2064", "password": "1LinarcTester@"}
    elif host == "qa-api.linarc.io":
        payload = {"username": "karlj8538", "password": "1LinarcTester@"}
    headers = {
        "Content-Type": "application/json",
    }

    response = requests.request("POST", url, headers=headers, data=json.dumps(payload))

    response = response.json()

    return response["access"]
