import os
from pdf2image import convert_from_bytes
from web_hooks import post_trigger_generated_file, update_status


def pdf_to_image(web_hook_info, s3_client, s3_bucket, pdf_file, s3_parent_file_key):

    parent_file_content = s3_parent_file_key.split(".")[-1]
    # print(parent_file_content)

    if parent_file_content in ("pdf", "zip"):
        s3_key = s3_parent_file_key.split(".")[:-1]
        s3_key.append("jpg")
        s3_key = ".".join(s3_key)
        print(s3_key)

    else:
        raise Exception("Cannot parse non pdf file")

    directory = "/tmp/images/"
    if not os.path.exists(directory):
        os.mkdir(directory)

    images = convert_from_bytes(
        pdf_file,
        output_folder="/tmp/images/",
        poppler_path="/opt/bin/",
        fmt="jpeg",
        jpegopt={"quality": 100, "progressive": True, "optimize": True},
    )

    for idx, img in enumerate(images):
        image_directory = f"/tmp/image-{idx}.jpg"
        width = img.width
        height = img.height
        img.save(image_directory, "jpeg")

    batch_id = s3_parent_file_key.split("/")[-2]
    plf_uploaded_file_name = s3_key.split("/")[-1]
    payload = [
        {
            "plf_parent": s3_parent_file_key,
            "plf_s3_key": s3_key,
            "plf_user_uploaded": "False",
            "plf_uploaded_file_name": f"{plf_uploaded_file_name}-{idx}.jpg",
            "plf_sheet_data": {
                "width": width,
                "height": height,
            },
        }
    ]
    print(payload)
    post_trigger_generated_file(web_hook_info, batch_id, payload)

    try:
        s3_client.upload_file(
            image_directory,
            Bucket=s3_bucket,
            Key=s3_key,
            ExtraArgs={
                "ContentType": "image/jpeg",
                "Metadata": {
                    "lambda_upload": "TRUE",
                    "s3_parent_file_key": s3_parent_file_key,
                    "uploaded_host": web_hook_info["file_uploaded_host"],
                },
            },
        )
    except Exception as e:
        print(e)
        update_status(web_hook_info, s3_key, status="FAL")
