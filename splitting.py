from PyPDF2 import PdfFileWriter
from web_hooks import post_trigger_generated_file, update_status


def split_files(web_hook_info, inputpdf, s3_client, s3_bucket, s3_parent_file_key):
    uploaded_files = []
    s3_key_main = ".".join(s3_parent_file_key.split(".")[:-1])

    for i in range(inputpdf.numPages):

        output = PdfFileWriter()
        output.addPage(inputpdf.getPage(i))

        file_path = "/tmp/document-page-%s.pdf" % i
        with open(file_path, "wb") as outputStream:
            output.write(outputStream)

        file_name = s3_parent_file_key.split("/")[-1] + "document-page-%s.pdf" % i
        print("file_name", file_name)

        splitted_file_data = {
            "s3_key": s3_key_main + "document-page-%s.pdf" % i,
            "file_name": file_name,
            "file_path": file_path,
        }

        uploaded_files.append(splitted_file_data)

    print("uploaded_files", uploaded_files)

    if uploaded_files:
        batch_id = s3_parent_file_key.split("/")[-2]
        payload = []
        for file in uploaded_files:
            info = {
                "plf_parent": s3_parent_file_key,
                "plf_s3_key": file["s3_key"],
                "plf_user_uploaded": "False",
                "plf_uploaded_file_name": file["file_name"],
            }
            payload.append(info)
        # print(payload)
        post_trigger_generated_file(web_hook_info, batch_id, payload)

        for file in uploaded_files:
            try:
                s3_client.upload_file(
                    file["file_path"],
                    Bucket=s3_bucket,
                    Key=file["s3_key"],
                    ExtraArgs={
                        "ContentType": "application/pdf",
                        "Metadata": {
                            "lambda_upload": "TRUE",
                            "s3_parent_file_key": s3_parent_file_key,
                            "uploaded_host": web_hook_info["file_uploaded_host"],
                        },
                    },
                )
                print("successfull")
            except Exception as e:
                print(e)
                update_status(web_hook_info, file["s3_key"], status="FAL")

    # a = os.listdir("/tmp")
    # for x in a:
    #     print(x)
