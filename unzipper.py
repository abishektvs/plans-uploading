import zipfile
import uuid
import mimetypes
from io import BytesIO
from web_hooks import update_status
from web_hooks import post_trigger_generated_file


content_type_map = {
    "application/pdf": "pdf",
    "image/jpeg": "jpeg",
    "image/jpg": "jpg",
    "application/zip": "zip",
}


def unzip(web_hook_info, s3_client, obj, bucket, s3_parent_file_key):
    print("unzip")
    # uploaded_files = []

    with BytesIO(obj["Body"].read()) as bytefile:
        # rewind the file
        bytefile.seek(0)

        files_to_upload = []
        failed_files = []

        # Read the file as a zipfile and process the members
        zipf = zipfile.ZipFile(bytefile, mode="r")
        for file in zipf.infolist():
            file_name = file.filename
            is_directory = file_name.endswith("/")
            file_content_type = mimetypes.guess_type(file_name)[0]
            file_type = content_type_map.get(file_content_type, None)
            if is_directory:
                dir_name = file_name
                continue

            unzipped_data = {
                "file_name": file_name,
                "is_directory": is_directory,
                "file_content_type": file_content_type,
                "file_type": file_type,
                "file": file,
            }

            if file_content_type is not None and file_content_type in content_type_map:
                charaters = []
                for idx, char in enumerate(file_name):
                    if idx < len(dir_name) and dir_name[idx] == char:
                        continue
                    else:
                        charaters.append(char)

                file_name = "".join(charaters)
                s3_key = s3_parent_file_key.split("/")[:-1]
                s3_key = "/".join(s3_key)
                s3_key = s3_key + "/" + str(uuid.uuid4()) + "." + file_type

                unzipped_data["s3_key"] = s3_key
                unzipped_data["status"] = "UPD"

                files_to_upload.append(unzipped_data)
                # print(unzipped_data)

            else:
                failed_files.append(unzipped_data)

        print("files_to_upload", files_to_upload)

        if files_to_upload:
            batch_id = s3_parent_file_key.split("/")[-2]
            payload = []
            for file in files_to_upload:
                info = {
                    "plf_parent": s3_parent_file_key,
                    "plf_s3_key": file["s3_key"],
                    "plf_user_uploaded": "False",
                    "plf_uploaded_file_name": file["file_name"],
                }
                payload.append(info)
            post_trigger_generated_file(web_hook_info, batch_id, payload)

            for file in files_to_upload:
                try:
                    s3_client.upload_fileobj(
                        zipf.open(file["file"]),
                        Bucket=bucket,
                        Key=file["s3_key"],
                        ExtraArgs={
                            "ContentType": file["file_content_type"],
                            "Metadata": {
                                "lambda_upload": "TRUE",
                                "s3_parent_file_key": s3_parent_file_key,
                                "uploaded_host": web_hook_info["file_uploaded_host"],
                            },
                        },
                    )

                except Exception as e:
                    print(e)
                    # raise Exception("Unsuccesful unzipped file upload")
                    update_status(web_hook_info, s3_key, status="FAL")


#               file_name = file.filename
#                 is_directory = file_name.endswith('/')
#                 file_content_type = mimetypes.guess_type(file_name)[0]
#                 file_type = content_type_map.get(file_content_type, None)
#                 print(file_name, is_directory)
#                 print(file_content_type, file_type)


#                 if is_directory:
#                     dir_name = file_name
#                     continue

#                 elif (
#                     file_content_type is not None and
#                     file_content_type in content_type_map
#                 ):
#                     charaters = []
#                     for idx, char in enumerate(file_name):
#                         if idx < len(dir_name) and dir_name[idx] == char:
#                             continue
#                         else:
#                             charaters.append(char)

#                     file_name = "".join(charaters)
#                     s3_key = s3_parent_file_key.split('/')[:-1]
#                     s3_key = "/".join(s3_key)
#                     s3_key = s3_key + "/" + str(uuid.uuid4()) + '.' + file_type
#                     print(s3_key)
#                     print(file_name)
#                     print()

#                     uploaded_files.append(
#                         {
#                             "file_name":file_name,
#                             "s3_key": s3_key
#                         }
#                     )


#                     try:
#                         print(2, file_content_type)
#                         s3_client.upload_fileobj(
#                             zipf.open(file),
#                             Bucket=bucket,
#                             Key=s3_key,
#                             ExtraArgs={"ContentType": file_content_type, "Metadata":
#                                   {"lambda_upload": "TRUE", "s3_parent_file_key":s3_parent_file_key}},
#                         )

#                     except:
#                         raise Exception("Unsuccesful unzipped file upload")
#                 else:
#                     raise Exception("Unaccepted File format Found")


#             if uploaded_files:
#                 batch_id = s3_parent_file_key.split('/')[-2]
#                 payload = []
#                 for file in uploaded_files:
#                     info = {
#                         "plf_parent": s3_parent_file_key,
#                         "plf_s3_key": file["s3_key"],
#                         "plf_user_uploaded": "False",
#                         "plf_uploaded_file_name": file["file_name"]
#                     }
#                     payload.append(info)
#                 post_trigger_generated_file(batch_id, payload)


# # Delete zip file after unzip
# if len(putObjects) > 0:
#     deletedObj = s3_client.delete_object(Bucket=bucket, Key=key)
#     print("deleted file:")
#     print(deletedObj)
