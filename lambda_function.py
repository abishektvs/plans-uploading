import json
import urllib.parse
import boto3
import mimetypes

# import requests
from PIL import Image
from io import BytesIO
from unzipper import unzip
from splitting import split_files
from imagify_pdf import pdf_to_image
from process import process_data, process_textract, process_sheet_index
from PyPDF2 import PdfFileReader
from web_hooks import (
    update_status,
    plan_file_detail,
    update_set_index_data,
    get_user_token,
)


accepted_content_type = {
    "application/pdf": "pdf",
    "image/jpeg": "jpeg",
    "image/jpg": "jpg",
    "application/zip": "zip",
    "application/json": "json",
}


def lambda_handler(event, context):
    # print("event ",event)
    # print("context ",context)
    try:
        s3_client = boto3.client("s3")
        s3_key = urllib.parse.unquote_plus(
            event["Records"][0]["s3"]["object"]["key"], encoding="utf-8"
        )
        print("s3_key", s3_key)
        s3_bucket = event["Records"][0]["s3"]["bucket"]["name"]
        file_obj = get_s3_object(s3_client, s3_key, s3_bucket)

        # file_uploaded_host = plan_file_detail(web_hook_info, s3_key)["plf_uploaded_host"]
        # print("file_detail", file_detail)

        file_uploaded_host = file_obj["Metadata"].get("uploaded_host", None)
        print(file_uploaded_host)

        user_token = get_user_token(file_uploaded_host)
        print("user_token", user_token)

        web_hook_info = {
            "user_token": user_token,
            "file_uploaded_host": file_uploaded_host,
        }

        print(file_obj)
        # Check the content type of uploaded file ie; image/jpeg, image/png;
        content_type = file_obj["ContentType"]
        content_type = mimetypes.guess_type(s3_key)[0]
        print(content_type)

        # Check user_uploaded or lambda_triggered file
        lambda_upload = file_obj["Metadata"].get("lambda_upload", "FALSE")
        print("meta", lambda_upload)
        if lambda_upload != "TRUE":
            print("user_uploaded_file")
            s3_parent_file_key = s3_key
            print("s3_parent_file_key", s3_parent_file_key)
            update_status(web_hook_info, s3_key, status="UPD")
        else:
            print("lambda_uploaded_file")
            s3_parent_file_key = file_obj["Metadata"].get("s3_parent_file_key", "None")
            print("s3_parent_file_key", s3_parent_file_key)

        if content_type == "application/zip":
            update_status(web_hook_info, s3_key, status="PCS")
            unzip(web_hook_info, s3_client, file_obj, s3_bucket, s3_key)

        elif content_type == "application/pdf":
            update_status(web_hook_info, s3_key, status="PCS")
            fs = file_obj["Body"].read()
            inputpdf = PdfFileReader(BytesIO(fs))
            print(inputpdf)
            number_of_pages = inputpdf.getNumPages()
            print(number_of_pages)
            if number_of_pages > 1:
                split_files(web_hook_info, inputpdf, s3_client, s3_bucket, s3_key)
            else:
                pdf_to_image(web_hook_info, s3_client, s3_bucket, fs, s3_key)
            print("done")

        elif content_type in ["image/jpeg", " image/jpg"]:
            update_status(web_hook_info, s3_key, status="PCS")

            # image height and width
            file_detail = plan_file_detail(web_hook_info, s3_key)
            file_sheet_data = file_detail["plf_sheet_data"]

            file_binary = file_obj["Body"].read()
            stream = BytesIO(file_binary)
            image = Image.open(stream)
            image_width, image_height = image.size
            if not file_sheet_data:
                file_sheet_data = {"width": image_width, "height": image_height}
            else:
                file_sheet_data["width"] = image_width
                file_sheet_data["height"] = image_height

            if s3_key.split(".")[-1] in ["pdf", "jpeg", "jpg", "zip"]:
                new_s3_key = s3_key.split(".")[:-1]
                new_s3_key = "".join(new_s3_key)
            else:
                raise Exception("Unaccepted File format Found")

            # process in textract and store in s3
            textract_client = boto3.client("textract")
            file_path, blocks = process_textract(textract_client, file_binary)
            update_status(
                web_hook_info,
                s3_key,
                status="DON",
                usable_file="True",
                plf_sheet_data=file_sheet_data,
            )

            s3_client.upload_file(
                file_path,
                Bucket=s3_bucket,
                Key=new_s3_key + ".json",
                ExtraArgs={
                    "ContentType": "application/json",
                    "Metadata": {
                        "lambda_upload": "TRUE",
                        "s3_parent_file_key": s3_key,
                        "uploaded_host": web_hook_info["file_uploaded_host"],
                    },
                },
            )
            print("textract done")

            # extract usefull data and store in s3
            processed_json_data, processed_json_file = process_data(blocks)
            is_sheet_index = processed_json_data["is_sheet_index"]

            s3_client.upload_file(
                "/tmp/processed.json",
                Bucket=s3_bucket,
                Key=new_s3_key + "processed.json",
                ExtraArgs={
                    "ContentType": "application/json",
                    "Metadata": {
                        "lambda_upload": "TRUE",
                        "s3_parent_file_key": s3_key,
                        "is_sheet_index": str(is_sheet_index),
                        "raw_textract_json_file_key": new_s3_key + ".json",
                        "uploaded_host": web_hook_info["file_uploaded_host"],
                    },
                },
            )
            sheet_number = processed_json_data["page_number"]
            sheet_number = sheet_number[0] if len(sheet_number) > 0 else ""
            file_sheet_data.update(processed_json_data)
            update_status(
                web_hook_info,
                s3_key,
                usable_file="True",
                plf_sheet_data=file_sheet_data,
                plf_sheet_number=sheet_number,
            )

        elif content_type == "application/json":
            is_sheet_index = file_obj["Metadata"].get("is_sheet_index", None)
            print(file_obj["Metadata"])
            print("s3_key", s3_key)
            print("is_sheet_index", is_sheet_index)
            if is_sheet_index is not None and is_sheet_index == "True":
                textract_json_file_key = file_obj["Metadata"].get(
                    "raw_textract_json_file_key", "DID not get"
                )
                print("textract_json_file_key", textract_json_file_key)
                raw_file_object = get_s3_object(
                    s3_client, textract_json_file_key, s3_bucket
                )
                raw_file = raw_file_object["Body"].read().decode("utf-8")
                blocks = json.loads(raw_file)

                file_content = file_obj["Body"].read().decode("utf-8")
                json_content = json.loads(file_content)

                sheet_names = process_sheet_index(
                    blocks, json_content["other_page_references"]
                )
                print(sheet_names)

                psm_id = s3_key.split("/")[-3]
                print(psm_id)
                update_set_index_data(web_hook_info, psm_id, sheet_names)

        else:
            deletedObj = s3_client.delete_object(Bucket=s3_bucket, Key=s3_key)
            print("deleted file")
            print(deletedObj)

    except Exception as e:
        print("failed")
        print(e)
        # update_status(web_hook_info, s3_key, status="FAL")
        raise e


def get_s3_object(s3_client, s3_key, s3_bucket):
    try:
        file_obj = s3_client.get_object(
            Key=s3_key,
            Bucket=s3_bucket,
        )
        print(file_obj)
        return file_obj
    except Exception as e:
        print(e)
        print(
            "Error getting object {} from bucket {}. \
            Make sure they exist and your bucket is in the \
            same region as this function.".format(
                s3_key, s3_bucket
            )
        )
        # update_status(web_hook_info, s3_key, status="FAL")
