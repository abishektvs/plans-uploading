import re
import json


def process_data(blocks):
    WORD = [i for i in blocks if i["BlockType"] == "WORD"]
    # WORD_TEXT = [i["Text"] for i in WORD]

    WORD_MAP = {}
    for i in WORD:
        if i["Text"] in WORD_MAP:
            WORD_MAP[i["Text"]].append(i)
        else:
            WORD_MAP[i["Text"]] = [i]

    data = analyze(WORD)
    # sheet_index = is_sheet_index(blocks)
    print(data)
    json_object = json.dumps(data, indent=4)
    file_path = "/tmp/processed.json"

    with open(file_path, "w") as outfile:
        outfile.write(json_object)

    return data, file_path


def process_textract(textract_client, file_binary):

    # process using S3 object
    response = textract_client.detect_document_text(Document={"Bytes": file_binary})

    blocks = response["Blocks"]
    # print(blocks)
    json_object = json.dumps(blocks, indent=4)
    file_path = "/tmp/sample.json"

    with open(file_path, "w") as outfile:
        outfile.write(json_object)

    return file_path, blocks


def analyze(WORD):

    page_number = []
    other_page_references = []
    max_volume = 0
    sheet_index = False

    for i in WORD:
        text = i["Text"]

        sheet_index = is_sheet_index(text) if sheet_index is not True else True

        # match = re.match(r"^[A-Z]+.*\d$", text)
        match = re.match(
            r"([A-Z]{1,3}(-)?[\d]{1,3}((\.|\-)[\d]{1,3}[A-Z]{0,3})*)$", text
        )
        if match and len(match[0]) < 8:
            left = i["Geometry"]["BoundingBox"]["Left"]
            top = i["Geometry"]["BoundingBox"]["Top"]
            prev_page_number_len = len(page_number)

            if left < 0.1:
                if top < 0.1:
                    page_number.append(i["Text"])
                elif top > 0.9:
                    page_number.append(i["Text"])
                else:
                    other_page_references.append(i)
            elif left > 0.9:
                if top < 0.1:
                    page_number.append(i["Text"])
                elif top > 0.9:
                    page_number.append(i["Text"])
                else:
                    other_page_references.append(i)
            else:
                other_page_references.append(i)

            # to check any page_number added and replace with larger in space occupied
            if len(page_number) > prev_page_number_len:
                height = i["Geometry"]["BoundingBox"]["Height"]
                width = i["Geometry"]["BoundingBox"]["Width"]
                volume = height * width
                if volume > max_volume:
                    page_number = [text]
                    max_volume = volume

    other_page_references_list = list(map(lambda x: x["Text"], other_page_references))
    other_page_references_data = list(
        map(lambda x: {x["Text"]: x["Geometry"]}, other_page_references)
    )

    return {
        "page_number": page_number,
        "other_page_references": other_page_references_list,
        "other_page_references_data": other_page_references_data,
        "is_sheet_index": sheet_index,
    }


def is_sheet_index(text):
    text = text.lower()
    if text in [
        "plans index",
        "sheet index",
        "sheet_index",
        "plans_index",
        "page_index",
        "index",
    ]:
        return True
    else:
        return False


def process_sheet_index(blocks, other_page_references):
    LINE = [block for block in blocks if block["BlockType"] == "LINE"]
    LINE_TEXT = [i["Text"] for i in LINE]

    sheet_name = {}
    for idx, text in enumerate(LINE_TEXT):
        words = text.split()
        for word in words:
            if word in other_page_references:
                p = text.split()
                if len(p) <= 1:
                    next_line = LINE_TEXT[idx + 1] if idx + 1 < len(LINE_TEXT) else text
                    if text in sheet_name:
                        sheet_name[text].append(next_line)
                    else:
                        sheet_name[text] = [next_line]
                else:
                    if word in sheet_name:
                        sheet_name[word].append(text)
                    else:
                        sheet_name[word] = [text]
    return sheet_name
